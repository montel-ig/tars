module gitwatcher

go 1.14

require (
	github.com/gomodule/redigo v1.8.1
	github.com/xanzy/go-gitlab v0.28.0
)

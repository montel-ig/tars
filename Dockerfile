# build stage
FROM golang:alpine as builder

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /out/run

# final stage
FROM alpine
LABEL maintainer="constanza@montel.fi"

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

COPY --from=builder /out/ /out/

EXPOSE 8080
CMD ["/out/run"]

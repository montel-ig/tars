package watcher

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"gitwatcher/providers"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"reflect"
	"testing"
	"time"
)

func setup() *Watcher {
	provider := providers.NewFakeProvider("test_data/watcher/repository.json")
	pool := &redis.Pool{
		MaxIdle:		5,
		IdleTimeout: 	240 * time.Second,
		Dial: 			func() (redis.Conn, error) {
			return redis.Dial("tcp", os.Getenv("REDIS_URL"))
		},
	}
	return NewWatcher(provider, pool)
}

func cleanUp(conn redis.Conn) {
	_, err := conn.Do("FLUSHDB")

	if err != nil {
		log.Printf("Error cleaning up database!")
	}
	conn.Close()
}

func getPushEvent() *providers.GitPushEvent {
	request, _ := http.NewRequest("POST", "", nil)
	request.Header.Add("Event-Type", "push")
	payload, _ := ioutil.ReadFile("test_data/watcher/push_event.json")

	pushEvent, _ := providers.NewFakeProvider("").ParseWebhook(request, payload)

	return pushEvent.(*providers.GitPushEvent)
}

func getPipelineEvent(i int) *providers.GitPipelineEvent {
	request, _ := http.NewRequest("POST", "", nil)
	request.Header.Add("Event-Type", "pipeline")
	payload, _ := ioutil.ReadFile(fmt.Sprintf("test_data/watcher/pipeline_event_%v.json", i))

	pipelineEvent, _ := providers.NewFakeProvider("").ParseWebhook(request, payload)

	return pipelineEvent.(*providers.GitPipelineEvent)
}

func TestWatcher_extractIssues(t *testing.T) {
	watcher := setup()
	pushEvent := getPushEvent()

	tests := []struct {
		staging	bool
		commits []int
	}{
		{
			true,
			[]int{1, 3},
		},
		{
			false,
			[]int{1, 2, 3},
		},
	}

	for _, test := range tests {
		commits := watcher.extractIssues(pushEvent.Commits, test.staging)
		if !reflect.DeepEqual(test.commits, commits) {
			t.Errorf("Watcher.extractIssues returned %+v, want %+v", commits, test.commits)
		}
	}
}

func TestWatcher_mustUpdate(t *testing.T) {
	watcher := setup()
	labels := map[string]providers.GitLabel{
		"staging": {
			Name: "Stage",
		},
		"doing": {
			Name: "Doing",
		},
	}

	tests := []struct {
		projectId	int
		issueId		int
		labels		map[string]providers.GitLabel
		staging		bool
		result		bool
	}{
		{
			3,
			1,
			labels,
			true,
			true,
		},
		{
			3,
			1,
			labels,
			false,
			false,
		},
		{
			3,
			2,
			labels,
			true,
			true,
		},
		{
			3,
			2,
			labels,
			false,
			true,
		},
		{
			3,
			3,
			labels,
			false,
			false,
		},
		{
			3,
			3,
			labels,
			false,
			false,
		},
	}

	for _, test := range tests {
		mustUpdate, _ := watcher.mustUpdate(test.projectId, test.issueId, test.labels, test.staging)
		if mustUpdate != test.result {
			t.Errorf("Watcher.mustUpdate returned %v, want %v", mustUpdate, test.result)
		}
	}
}

func TestWatcher_projectLabels(t *testing.T) {
	watcher := setup()

	labels := map[string]providers.GitLabel{
		"staging": {
			3,
			"Stage",
			"#d9534f",
			"",
		},
		"doing": {
			3,
			"Doing",
			"#d9534f",
			"",
		},
	}

	resultLabels, _ := watcher.projectLabels(3)
	if !reflect.DeepEqual(labels, resultLabels) {
		t.Errorf("Watcher.projectLabels returned %v, want %v", resultLabels, labels)
	}
}

func TestWatcher_updateIssues(t *testing.T) {
	watcher := setup()

	testsFeature := []struct {
		projectId	int
		issueId		int
		labels		[]string
	} {
		{
			3,
			1,
			[]string{"Doing", "High Priority"},
		},
		{
			3,
			2,
			[]string{"Doing"},
		},
		{
			3,
			3,
			[]string{"Stage"},
		},
	}

	testsStaging := []struct {
		projectId	int
		issueId		int
		labels		[]string
	} {
		{
			3,
			1,
			[]string{"High Priority", "Stage"},
		},
		{
			3,
			2,
			[]string{"Doing"},
		},
		{
			3,
			3,
			[]string{"Stage"},
		},
	}

	watcher.updateIssues(3, []int{1, 2, 3}, false)

	for _, test := range testsFeature {
		issue, _ := watcher.Prov.GetIssue(test.projectId, test.issueId)
		if !reflect.DeepEqual(test.labels, issue.Labels) {
			t.Errorf("Issue %v returned labels %v, want %v", test.issueId, issue.Labels, test.labels)
		}
	}

	watcher.updateIssues(3, []int{1, 3}, true)

	for _, test := range testsStaging {
		issue, _ := watcher.Prov.GetIssue(test.projectId, test.issueId)
		if !reflect.DeepEqual(test.labels, issue.Labels) {
			t.Errorf("Issue %v returned labels %v, want %v", test.issueId, issue.Labels, test.labels)
		}
	}
}

func TestWatcher_saveIssues(t *testing.T) {
	watcher := setup()
	pushEventFeature := getPushEvent()
	conn := watcher.DB.Get()
	defer cleanUp(conn)

	testKey := fmt.Sprintf("%v:%v", pushEventFeature.ProjectName, pushEventFeature.CheckoutSHA)
	testValue := "1 3"

	watcher.saveIssues(*pushEventFeature)

	exists, _ := redis.Int(conn.Do("EXISTS", testKey))

	if exists == 0 {
		t.Errorf("%v doesn't exist in the database", testKey)
	}

	value, _ := redis.String(conn.Do("GET", testKey))

	if value != testValue {
		t.Errorf("%v saved to the database, wanted %v", value, testValue)
	}

	// Test recovering previous issues
	prevKey := fmt.Sprintf("%v:%v", pushEventFeature.ProjectName, pushEventFeature.Before)
	prevValues := "5 6"
	expectedValues := "5 6 1 3"
	_, err := conn.Do("SET", prevKey, prevValues)

	if err != nil {
		t.Errorf("%v", err)
	}

	watcher.saveIssues(*pushEventFeature)

	value, _ = redis.String(conn.Do("GET", testKey))

	if value != expectedValues {
		t.Errorf("%v saved to the database, wanted %v", value, expectedValues)
	}
}

func TestWatcher_getIssuesFromDB(t *testing.T) {
	watcher := setup()
	pushEventFeature := getPushEvent()
	pipelineEvent := getPipelineEvent(1)

	testKey := fmt.Sprintf("%v:%v", pushEventFeature.ProjectName, pushEventFeature.CheckoutSHA)
	testValue := []int{1, 3}

	conn := watcher.DB.Get()
	defer cleanUp(conn)

	_, err := conn.Do("SET", testKey, testValue)

	if err != nil {
		t.Errorf("%v", err)
	}

	value := watcher.getIssuesFromDB(pipelineEvent.ProjectName, pipelineEvent.Commit.ID)

	if !reflect.DeepEqual(testValue, value) {
		t.Errorf("Got value from db %v, wanted %v", value, testValue)
	}
}

func TestWatcher_checkPassedPipeline(t *testing.T) {
	watcher := setup()

	tests := []struct {
		pipeline	int
		result		bool
	}{
		{
			1,
			true,
		},
		{
			2,
			false,
		},
	}

	for _, test := range tests {
		pipelineEvent := getPipelineEvent(test.pipeline)
		result := watcher.checkPassedPipeline(*pipelineEvent)
		if result != test.result {
			t.Errorf("Watcher.checkPassedPipeline returned %v for pipeline %v, want %v", result, test.pipeline, test.result)
		}
	}
}

func TestWatcher_targetLabels(t *testing.T) {
	watcher := setup()

	tests := []struct {
		new		string
		old		string
		labels	[]string
		result	[]string
	}{
		{
			"",
			"",
			[]string{"Doing", "High Priority"},
			[]string{"Doing", "High Priority"},
		},
		{
			"Doing",
			"",
			[]string{"High Priority"},
			[]string{"High Priority", "Doing"},
		},
		{
			"Staging",
			"Doing",
			[]string{"Doing", "High Priority"},
			[]string{"High Priority", "Staging"},
		},
		{
			"Staging",
			"Doing",
			[]string{"High Priority"},
			[]string{"High Priority", "Staging"},
		},
	}

	for _, test := range tests {
		labels := watcher.targetLabels(test.new, test.old, test.labels)
		if !reflect.DeepEqual(labels, test.result) {
			t.Errorf("Watcher.targetLabels returned %v, want %v", labels, test.result)
		}
	}
}
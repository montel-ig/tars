package watcher

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"gitwatcher/providers"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

var issueRegexp = regexp.MustCompile(`#\d+`)
var numberRegexp = regexp.MustCompile(`\d+`)
var closeIssueRegexp = regexp.MustCompile(`((?:[Cc]los(?:e[sd]?|ing)|[Ff]ix(?:e[sd]|ing)?|[Rr]esolv(?:e[sd]?|ing)|[Ii]mplement(?:s|ed|ing)?)(:?) +(?:(?:issues? +)?#\d+(?:(?:,? *| +and +)?)|([A-Z][A-Z0-9_]+-\d+))+)`)

type Watcher struct {
	Prov	providers.GitProvider
	DB		*redis.Pool
	Errors	chan error
}

func NewWatcher(prov providers.GitProvider, db *redis.Pool) *Watcher {
	return &Watcher{
		Prov:	prov,
		DB:		db,
		Errors: make(chan error),
	}
}

func (w Watcher) HandleWebhook(rw http.ResponseWriter, r *http.Request) {
	payload, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		log.Printf("error reading request body: err=%s\n", err)
		return
	}

	if !w.Prov.VerifyEvent(r, payload) {
		http.Error(rw, "incorrect token", http.StatusBadRequest)
		return
	}

	event, err := w.Prov.ParseWebhook(r, payload)

	if err != nil {
		log.Printf("could not parse webhook: err=%s\n", err)
		return
	}

	switch e := event.(type) {
	case *providers.GitPushEvent:
		log.Printf("%s pushed to repository %s", e.UserName, e.ProjectName)
		go w.handlePush(*e)
	case *providers.GitPipelineEvent:
		log.Printf("Pipeline update at %s", e.ProjectName)
		go w.handlePipeline(*e)
	default:
		log.Printf("Ignoring event type")
		return
	}
}

func (w Watcher) handlePush(e providers.GitPushEvent) {
	log.Printf("Pushed to branch %v", e.Branch)
	switch e.Branch {
	case "refs/heads/develop":
		w.saveIssues(e)
	case "refs/head/master":
	default:
		w.updateIssuesFromPush(e, false)
	}
}

func (w Watcher) handlePipeline(e providers.GitPipelineEvent) {
	switch e.Branch {
	case "develop":
		log.Printf("Pipeline in branch %v", e.Branch)
		w.updateIssuesFromPipeline(e)
	default:
		log.Printf("Ignored pipeline in branch %v", e.Branch)
	}
}

func (w Watcher) updateIssuesFromPush(e providers.GitPushEvent, staging bool) {
	issues := w.extractIssues(e.Commits, staging)
	w.updateIssues(e.ProjectID, issues, staging)
}

func (w Watcher) updateIssuesFromPipeline(e providers.GitPipelineEvent) {
	if !w.checkPassedPipeline(e) {
		log.Printf("Deployment not finished for project %v", e.ProjectName)
		return
	}
	issues := w.getIssuesFromDB(e.ProjectName, e.Commit.ID)
	w.updateIssues(e.ProjectID, issues, true)
}

func (w Watcher) updateIssues(p int, issues []int, staging bool) {
	labels, _ := w.projectLabels(p)
	var newLabel, old string

	if staging {
		newLabel = labels["staging"].Name
		old = labels["doing"].Name
	} else {
		newLabel = labels["doing"].Name
		old = ""
	}

	log.Printf("Target label %v", newLabel)

	for _, iss := range issues {
		log.Printf("Checking issue #%v", iss)
		if update, issueLabels := w.mustUpdate(p, iss, labels, staging); update {
			log.Printf("Must update")
			targetLabels := w.targetLabels(newLabel, old, issueLabels)
			_, err := w.Prov.EditIssueLabel(p, iss, targetLabels)

			if err != nil {
				log.Printf("Error!")
			} else {
				log.Printf("Success!")
			}
		} else {
			log.Printf("No update")
		}
	}
}

func (w Watcher) saveIssues(e providers.GitPushEvent) {
	conn := w.DB.Get()
	defer conn.Close()
	var issues []int

	oldKey := fmt.Sprintf("%v:%v", e.ProjectName, e.Before)
	exists, _ := redis.Int(conn.Do("EXISTS", oldKey))
	if exists == 1 {
		log.Printf("Getting issues from previous push with key %v", oldKey)
		issues = w.getIssuesFromDB(e.ProjectName, e.Before)
	}

	issues = append(issues, w.extractIssues(e.Commits, true)...)
	newIssues := strings.Trim(fmt.Sprint(issues), "[]")

	if newIssues == "" {
		log.Print("No issues to save")
		return
	}

	key := fmt.Sprintf("%s:%s", e.ProjectName, e.CheckoutSHA)

	_, err := conn.Do("SET", key, newIssues)

	if err != nil {
		log.Printf("Error!")
	} else {
		log.Printf("Saved successfully, value %v", issues)
	}
}

func (w Watcher) getIssuesFromDB(projectName string, commit string) []int {
	key := fmt.Sprintf("%s:%s", projectName, commit)
	conn := w.DB.Get()
	defer conn.Close()

	value, err := redis.String(conn.Do("GET", key))

	if err != nil {
		log.Printf("Error reading values from Redis! Key %v", key)
	}

	_, err = conn.Do("DEL", key)

	var issues []int
	for _, iss := range numberRegexp.FindAllString(value, -1) {
		issueNumber, _ := strconv.Atoi(iss)
		issues = append(issues, issueNumber)
	}
	return issues
}

func (w Watcher) projectLabels(p int) (map[string]providers.GitLabel, error) {
	var result = make(map[string]providers.GitLabel)

	labels, err := w.Prov.GetLabels(p)

	if err != nil {
		return nil, err
	}

	for _, label := range labels {
		if matched, _ := regexp.MatchString(`[Ss]tag(e|ing)`, label.Name); matched {
			result["staging"] = *label
		} else if matched, _ := regexp.MatchString(`[Dd]oing`, label.Name); matched {
			result["doing"] = *label
		}
	}
	return result, nil
}

func (w Watcher) extractIssues(commits []*providers.GitCommit, staging bool) []int {
	var issues []string
	var result []int

	for _, commit := range commits {
		if staging {
			closeIssues := closeIssueRegexp.FindAllString(commit.Message, -1)
			for _, ci := range closeIssues {
				issues = append(issues, issueRegexp.FindAllString(ci, -1)...)
			}
		} else {
			issues = append(issues, issueRegexp.FindAllString(commit.Message, -1)...)
		}
	}

	for _, iss := range issues {
		issueNumber, _ := strconv.Atoi(strings.Trim(iss, "#"))
		result = append(result, issueNumber)
	}
	return result
}

func (w Watcher) mustUpdate(p int, iss int, labels map[string]providers.GitLabel, staging bool) (bool, []string) {
	issue, err := w.Prov.GetIssue(p, iss)

	if err != nil {
		log.Printf("Error %v\n", err)
		return false, nil
	}
	switch {
	case providers.StringInSlice(labels["staging"].Name, issue.Labels):
		log.Printf("Has label staging and is pushing to staging")
		return false, nil
	case providers.StringInSlice(labels["doing"].Name, issue.Labels) && !staging:
		log.Printf("Has label doing but is not pushing to staging")
		return false, nil
	default:
		log.Printf("Does not have label staging or doing")
		return true, issue.Labels
	}
}

func (w Watcher) checkPassedPipeline(e providers.GitPipelineEvent) bool {
	for _, job := range e.Builds {
		if job.Stage == "deploy" && job.Status == "success" {
			return true
		}
	}
	return false
}

func (w Watcher) targetLabels(new string, old string, labels []string) []string {
	result := providers.PopStringFromSlice(old, labels)
	if new != "" {
		result = append(result, new)
	}
	return result
}

#!/usr/bin/env sh

go test -c ./providers -o /out/tests/providers.test
go test -c ./watcher -o /out/tests/watcher.test
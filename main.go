package main

import (
	"github.com/gomodule/redigo/redis"
	"gitwatcher/providers"
	"gitwatcher/watcher"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	gitlabProvider, _ := providers.NewGitlabProvider(os.Getenv("GITLAB_ACCESS_TOKEN"), "")
	pool := &redis.Pool{
		MaxIdle:		5,
		IdleTimeout: 	240 * time.Second,
		Dial: 			func() (redis.Conn, error) {
			return redis.Dial("tcp", os.Getenv("REDIS_URL"))
		},
	}
	gitlabWatcher := watcher.NewWatcher(gitlabProvider, pool)

	log.Println("server started")
	http.HandleFunc("/gitlab/webhook", gitlabWatcher.HandleWebhook)
	http.HandleFunc("/healthcheck", func(w http.ResponseWriter, req *http.Request) {
		io.WriteString(w, "healthy\n")
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}

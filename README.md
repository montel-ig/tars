## GitWatcher :eyes:

Small Go project to manage issue :memo: labels for different projects, using webhooks and the repo service API. 
Only Gitlab is supported currently, but extending for different platforms should be easy. The `providers` abstraction is based on [this library](https://github.com/artur-sak13/gitmv).

A Redis service must be running alongside, to save pending push events.

Environment variables needed:
```text
GITLAB_KEY_<PROJECT PATH WITH NAMESPACE>=<gitlab payload secret>
GITLAB_ACCESS_TOKEN=<access token for the account used>
REDIS_URL=<redis service>
```

To configure the webhook's [payload secret](https://gitlab.com/help/user/project/integrations/webhooks#secret-token) for each project, it must be defined in a environment variable with the format `GITLAB_KEY_<PROJECT PATH WITH NAMESPACE>`, with the path in all caps. 

So for example, for a repo named **Bar** hosted on Gitlab under de organization **Foo**, the environment variable would be `GITLAB_KEY_FOO_BAR`.
  
If the project is organized inside group **Foo-bar**, the variable would be `GITLAB_KEY_FOO_FOO-BAR_BAR` (Note the dash in the group's name. This also applies for dahes inside the organization or project name)
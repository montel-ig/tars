package providers

import (
	"fmt"
	"github.com/xanzy/go-gitlab"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"strings"
	"testing"
	"time"
)

func setupGitlab() (*http.ServeMux, *httptest.Server, *GitlabProvider) {
	mux := http.NewServeMux()

	server := httptest.NewServer(mux)

	c := gitlab.NewClient(nil, "")
	_ = c.SetBaseURL(server.URL)

	// Gitlab provider that we want to test
	prov := WithGitlabClient(c, "")

	return mux, server, prov.(*GitlabProvider)
}

func teardown(server *httptest.Server) {
	server.Close()
}

func configureMock(mux *http.ServeMux, fileSuffix string, urlSuffix string) {
	var filename, url string
	filename = fmt.Sprintf("test_data/gitlab/%s.json", fileSuffix)
	url = fmt.Sprintf("/api/v4/projects/4/%s", urlSuffix)

	mux.HandleFunc(url, func(w http.ResponseWriter, r *http.Request) {
		src, err := ioutil.ReadFile(filename)

		if err != nil {
			panic(err)
		}

		_, _ = w.Write(src)
	})
}

func setupGitlabWebhook(event string) (*http.Request, []byte) {
	_ = os.Setenv("GITLAB_KEY_MIKE_DIASPORA", "abc123")
	request, _ := http.NewRequest("POST", "", nil)
	request.Header.Add("X-Gitlab-Event", fmt.Sprintf("%v Hook", event))
	request.Header.Add("X-Gitlab-Token", "abc123")

	payload, _ := ioutil.ReadFile(fmt.Sprintf("test_data/gitlab/%v_event.json", strings.ToLower(event)))
	return request, payload
}

func TestGitlabProvider_GetIssue(t *testing.T) {
	mux, server, prov := setupGitlab()

	defer teardown(server)

	tests := []*GitIssue {
		{
			4,
			1,
			"Ut commodi ullam eos dolores perferendis nihil sunt.",
			"Omnis vero earum sunt corporis dolor et placeat.",
			"closed",
			[]string{},
			"Administrator",
		},
	}

	for i, test := range tests {
		filename := fmt.Sprintf("issue_%v", i)
		url := fmt.Sprintf("issues/%v", i)
		configureMock(mux, filename, url)

		issue, _ := prov.GetIssue(4, i)

		if !reflect.DeepEqual(test, issue) {
			t.Errorf("Issues.GetIssue returned %+v, want %+v", issue, test)
		}
	}
}

func TestGitlabProvider_GetLabels(t *testing.T) {
	mux, server, prov := setupGitlab()

	defer teardown(server)

	test := []*GitLabel {
		{
			4,
			"Todo",
			"#d9534f",
			"",
		},
		{
			4,
			"Stage",
			"#d9534f",
			"",
		},
		{
			4,
			"Doing",
			"#d9534f",
			"",
		},
	}
	configureMock(mux, "labels_1", "labels")

	labels, _ := prov.GetLabels(4)

	if !reflect.DeepEqual(test, labels) {
		t.Errorf("GitlabProvider.GetLabels returned %+v, want %+v", labels, test)
	}
}

func TestGitlabProvider_EditIssueLabel(t *testing.T) {
	mux, server, prov := setupGitlab()

	defer teardown(server)

	tests := []*GitIssue {
		{
			4,
			1,
			"Ut commodi ullam eos dolores perferendis nihil sunt.",
			"Omnis vero earum sunt corporis dolor et placeat.",
			"closed",
			[]string{},
			"Administrator",
		},
	}

	label := []string{"Doing"}

	for i, test := range tests {
		filename := fmt.Sprintf("issue_%v", i)
		url := fmt.Sprintf("issues/%v", i)
		configureMock(mux, filename, url)

		issue, _ := prov.EditIssueLabel(4, i, label)

		if !reflect.DeepEqual(test, issue) {
			t.Errorf("GitlabProvider.GetIssue returned %+v, want %+v", issue, test)
		}
	}
}

func TestGitlabProvider_ParseWebhook(t *testing.T) {
	_, server, prov := setupGitlab()

	defer teardown(server)

	request, payload := setupGitlabWebhook("Push")
	timestamp1, _ := time.Parse(time.RFC3339, "2011-12-12T14:27:31+02:00")
	timestamp2, _ := time.Parse(time.RFC3339, "2012-01-03T23:36:29+02:00")

	testPush := &GitPushEvent{
		ObjectKind:  "push",
		Before: 	 "95790bf891e76fee5e1747ab589903a6a1f80f22",
		CheckoutSHA: "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
		UserID:      4,
		UserName:    "John Smith",
		ProjectID:   4,
		ProjectName: "Diaspora",
		Branch:      "refs/heads/master",
		Commits:     []*GitCommit {
			{
				"b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327",
				"Update Catalan translation. Close #1, related to #2",
				&timestamp1,
				"Jordi Mallach",
				"jordi@softcatala.org",
			},
			{
				"da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
				"fixed readme, Close #2, #3",
				&timestamp2,
				"GitLab dev user",
				"gitlabdev@dv6700.(none)",
			},
		},
	}

	testPipeline := &GitPipelineEvent{
		ObjectKind:   "pipeline",
		UserName:     "Administrator",
		UserUserName: "root",
		ProjectID:    4,
		ProjectName:  "Diaspora",
		Branch:       "develop",
		Commit:       GitCommit{
			ID:          "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
			Message:     "test\n",
			Timestamp:   &timestamp1,
			AuthorName:  "User",
			AuthorEmail: "user@gitlab.com",
		},
		Builds:       []*GitBuild{
			{
				380,
				"deploy",
				"production",
				"success",
			},
			{
				377,
				"test",
				"test-image",
				"success",
			},
			{
				378,
				"test",
				"test-build",
				"success",
			},
			{
				376,
				"build",
				"build-image",
				"success",
			},
			{
				379,
				"deploy",
				"staging",
				"created",
			},
		},
	}

	event, _ := prov.ParseWebhook(request, payload)

	if !reflect.DeepEqual(testPush, event) {
		t.Errorf("GitlabProvider.ParseWebhook returned %+v, want %+v", event, testPush)
	}

	request, payload = setupGitlabWebhook("Pipeline")

	event, _ = prov.ParseWebhook(request, payload)

	if !reflect.DeepEqual(testPipeline, event) {
		t.Errorf("GitlabProvider.ParseWebhook returned %+v, want %+v", event, testPipeline)
	}
}

func TestGitlabProvider_VerifyEvent(t *testing.T) {
	_, server, prov := setupGitlab()
	defer teardown(server)

	request, payload := setupGitlabWebhook("Push")

	verified := prov.VerifyEvent(request, payload)

	if verified != true {
		t.Errorf("GitlabProvider.VerifyEvent returned %v, want %v", verified, true)
	}
}
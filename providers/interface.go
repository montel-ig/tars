package providers

import "net/http"

type GitProvider interface {
	// Read methods
	GetIssue(int, int) (*GitIssue, error)

	GetLabels(int) ([]*GitLabel, error)

	// Edit methods
	EditIssueLabel(int, int, []string) (*GitIssue, error)

	// Webhook methods
	VerifyEvent(*http.Request, []byte) bool

	ParseWebhook(*http.Request, []byte) (interface{}, error)
}

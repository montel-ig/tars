package providers

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/xanzy/go-gitlab"
	"net/http"
	"os"
	"strings"
)

// GitlabProvider implements the provider interface for GitLab
type GitlabProvider struct {
	Client  *gitlab.Client
	Context context.Context
	ID      string
}

// NewGitlabProvider creates a new GitLab client which implements the provider interface
func NewGitlabProvider(token string, url string) (GitProvider, error) {
	client := gitlab.NewClient(nil, token)
	if !IsHosted(url) {
		if err := client.SetBaseURL(url); err != nil {
			return nil, err
		}
	}
	return WithGitlabClient(client, token), nil
}

// IsHosted checks if the specified URL is a Git SaaS provider
func IsHosted(u string) bool {
	u = strings.TrimSuffix(u, "/")
	return u == "" || u == "https://gitlab.com" || u == "http://gitlab.com"
}

// WithGitlabClient creates a new GitProvider with a Gitlab client
// This function is exported to create mock clients in tests
func WithGitlabClient(client *gitlab.Client, token string) GitProvider {
	return &GitlabProvider{
		Client: client,
		ID:     token,
	}
}

func (g *GitlabProvider) VerifyEvent(r *http.Request, payload []byte) bool {
	payloadSecret := r.Header.Get("X-Gitlab-Token")
	projectKey := os.Getenv(fmt.Sprintf("GITLAB_KEY_%v", projectSuffix(payload)))

	return payloadSecret == projectKey
}

func (g *GitlabProvider) ParseWebhook(r *http.Request, payload []byte) (interface{}, error) {
	event, err := gitlab.ParseWebhook(gitlab.WebhookEventType(r), payload)

	if err != nil {
		return nil, err
	}

	switch e := event.(type) {
	case *gitlab.PushEvent:
		return &GitPushEvent{
			ObjectKind:  e.ObjectKind,
			Before:		 e.Before,
			CheckoutSHA: e.CheckoutSHA,
			UserID:      e.UserID,
			UserName:    e.UserName,
			ProjectID:   e.ProjectID,
			ProjectName: e.Project.Name,
			Branch:		 e.Ref,
			Commits:     toGitCommits(*e),
		}, nil
	case *gitlab.PipelineEvent:
		return &GitPipelineEvent{
			ObjectKind:   e.ObjectKind,
			UserName:     e.User.Name,
			UserUserName: e.User.Username,
			ProjectID:    e.Project.ID,
			ProjectName:  e.Project.Name,
			Branch:       e.ObjectAttributes.Ref,
			Commit:       GitCommit{
				ID:          e.Commit.ID,
				Message:     e.Commit.Message,
				Timestamp:   e.Commit.Timestamp,
				AuthorName:  e.Commit.Author.Name,
				AuthorEmail: e.Commit.Author.Email,
			},
			Builds:       toGitBuilds(*e),
		}, nil
	default:
		return nil, fmt.Errorf("can't parse event type %s", gitlab.WebhookEventType(r))
	}
}

func (g *GitlabProvider) GetIssue(p int, iss int) (*GitIssue, error) {
	result, _, err := g.Client.Issues.GetIssue(p, iss, nil)

	if err != nil {
		return nil, err
	}

	return &GitIssue{
		PID:       p,
		Number:    result.IID,
		Title:     result.Title,
		Body:      result.Description,
		State:     result.State,
		Labels:    result.Labels,
		User:      result.Author.Name,
	}, nil
}

func (g *GitlabProvider) GetLabels(p int) ([]*GitLabel, error) {
	var result []*GitLabel

	labels, _, err := g.Client.Labels.ListLabels(p, nil)

	if err != nil {
		return nil, err
	}

	for _, label := range labels {
		result = append(result, &GitLabel{
			Repo:        p,
			Name:        label.Name,
			Color:       label.Color,
			Description: label.Description,
		})
	}

	return result, nil
}

func (g GitlabProvider) EditIssueLabel(p int, issue int, labels []string) (*GitIssue, error) {
	issueLabels := gitlab.Labels{}

	for _, label := range labels {
		issueLabels = append(issueLabels, label)
	}
	ui := &gitlab.UpdateIssueOptions{
		Labels: &issueLabels,
	}

	result, _, err := g.Client.Issues.UpdateIssue(p, issue, ui)

	if err != nil {
		return nil, err
	}

	return &GitIssue{
		PID:       p,
		Number:    result.IID,
		Title:     result.Title,
		Body:      result.Description,
		State:     result.State,
		Labels:    result.Labels,
		User:      result.Author.Name,
	}, nil
}

func toGitCommits(e gitlab.PushEvent) []*GitCommit {
	var result []*GitCommit
	for _, commit := range e.Commits {
		result = append(result, &GitCommit{
			ID:          commit.ID,
			Message:     commit.Message,
			Timestamp:   commit.Timestamp,
			AuthorName:  commit.Author.Name,
			AuthorEmail: commit.Author.Email,
		})
	}
	return result
}

func toGitBuilds(e gitlab.PipelineEvent) []*GitBuild {
	var result []*GitBuild
	for _, build := range e.Builds {
		result = append(result, &GitBuild{
			ID:     build.ID,
			Stage:  build.Stage,
			Name:   build.Name,
			Status: build.Status,
		})
	}
	return result
}

func projectSuffix(payload []byte) string {
	var result map[string]interface{}
	json.Unmarshal(payload, &result)

	project := result["project"].(map[string]interface{})
	path := project["path_with_namespace"].(string)

	return strings.ToUpper(strings.ReplaceAll(path, "/", "_"))
}
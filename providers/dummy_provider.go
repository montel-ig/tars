package providers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type FakeRepository struct {
	RepoId      int
	Issues      map[int]*GitIssue
	Labels      []*GitLabel
}


type FakeProvider struct {
	Repositories map[int]*FakeRepository
}

func NewFakeProvider(filename string) GitProvider {
	provider := &FakeProvider{
		Repositories: make(map[int]*FakeRepository),
	}

	if filename != "" {
		id, repo := RepoFromFile(filename)
		provider.Repositories[id] = repo
	}

	return provider
}

func RepoFromFile(filename string) (int, *FakeRepository){
	src, err := ioutil.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	type rawEvent struct {
		Name   string `json:"name"`
		Id     int    `json:"id"`
		Issues []*struct {
			Id    int `json:"id"`
			Issue struct {
				PId    int      `json:"pid"`
				Id     int      `json:"id"`
				Title  string   `json:"title"`
				Body   string   `json:"body"`
				State  string   `json:"state"`
				Labels []string `json:"labels"`
				User   string   `json:"user"`
			} `json:"issue"`
		} `json:"issues"`
		Labels []*struct {
			Name        string `json:"name"`
			Repo        int    `json:"repo"`
			Description string `json:"description"`
			Color       string `json:"color"`
		}
	}

	repo := &rawEvent{}

	if err := json.Unmarshal(src, repo); err != nil {
		panic(err)
	}

	var issues = make(map[int]*GitIssue)
	var labels []*GitLabel

	for _, issue := range repo.Issues {
		issues[issue.Id] = &GitIssue{
			PID:    issue.Issue.PId,
			Number: issue.Issue.Id,
			Title:  issue.Issue.Title,
			Body:   issue.Issue.Body,
			State:  issue.Issue.State,
			Labels: issue.Issue.Labels,
			User:   issue.Issue.User,
		}
	}

	for _, label := range repo.Labels {
		labels = append(labels, &GitLabel{
			Repo:        label.Repo,
			Name:        label.Name,
			Color:       label.Color,
			Description: label.Description,
		})
	}

	return repo.Id, &FakeRepository{
		RepoId: repo.Id,
		Issues: issues,
		Labels: labels,
	}
}

func (f FakeProvider) GetIssue(p int, iss int) (*GitIssue, error) {
	repo, ok := f.Repositories[p]

	if !ok {
		return nil, fmt.Errorf("project '%v' not found", p)
	}

	issue, ok := repo.Issues[iss]

	if !ok {
		return nil, fmt.Errorf("issue '%v' not found in project '%v'", iss, p)
	}

	return issue, nil
}

func (f FakeProvider) GetLabels(p int) ([]*GitLabel, error) {
	repo, ok := f.Repositories[p]

	if !ok {
		return nil, fmt.Errorf("project '%v' not found", p)
	}

	return repo.Labels, nil
}

func (f FakeProvider) EditIssueLabel(p int, iss int, label []string) (*GitIssue, error) {
	repo, ok := f.Repositories[p]

	if !ok {
		return nil, fmt.Errorf("project '%v' not found", p)
	}

	issue, ok := repo.Issues[iss]

	if !ok {
		return nil, fmt.Errorf("issue '%v' not found in project '%v'", iss, p)
	}

	issue.Labels = label

	return issue, nil
}

//noinspection GoUnusedParameter
func (f FakeProvider) VerifyEvent(r *http.Request, payload []byte) bool {
	  return true
}

func (f FakeProvider) ParseWebhook(r *http.Request, payload []byte) (interface{}, error) {
	type pushEvent struct {
		ObjectKind	string	`json:"object_kind"`
		Before		string	`json:"before"`
		CheckoutSHA	string	`json:"checkout_sha"`
		UserID		int		`json:"user_id"`
		UserName	string	`json:"user_name"`
		ProjectID	int		`json:"project_id"`
		ProjectName	string	`json:"project_name"`
		Branch		string	`json:"branch"`
		Commits		[]*struct {
			ID			string		`json:"id"`
			Message		string		`json:"message"`
			Timestamp	*time.Time	`json:"timestamp"`
			AuthorName	string		`json:"author_name"`
			AuthorEmail	string		`json:"author_email"`
		} `json:"commits"`
	}

	type pipelineEvent struct {
		ObjectKind		string	`json:"object_kind"`
		UserName		string	`json:"user_name"`
		UserUserName	string	`json:"username"`
		ProjectID		int		`json:"project_id"`
		ProjectName		string	`json:"project_name"`
		Branch			string	`json:"object_attributes_ref"`
		Commit			struct {
			ID			string		`json:"id"`
			Message		string		`json:"message"`
			Timestamp	*time.Time	`json:"timestamp"`
			AuthorName	string		`json:"author_name"`
			AuthorEmail	string		`json:"author_email"`
		}	`json:"commit"`
		Builds			[]*struct {
			ID		int		`json:"id"`
			Stage	string	`json:"stage"`
			Name	string	`json:"name"`
			Status	string	`json:"status"`
		}	`json:"builds"`
	}

	switch r.Header.Get("Event-Type") {
	case "push":
		event := &pushEvent{}
		if err := json.Unmarshal(payload, event); err != nil {
			panic(err)
		}
		var commits []*GitCommit
		for _, commit := range event.Commits {
			commits = append(commits, &GitCommit{
				ID:          commit.ID,
				Message:     commit.Message,
				Timestamp:   commit.Timestamp,
				AuthorName:  commit.AuthorName,
				AuthorEmail: commit.AuthorEmail,
			})
		}
		result := &GitPushEvent{
			ObjectKind:  event.ObjectKind,
			Before: 	 event.Before,
			CheckoutSHA: event.CheckoutSHA,
			UserID:      event.UserID,
			UserName:    event.UserName,
			ProjectID:   event.ProjectID,
			ProjectName: event.ProjectName,
			Branch:      event.Branch,
			Commits:     commits,
		}
		return result, nil
	case "pipeline":
		event := &pipelineEvent{}
		if err := json.Unmarshal(payload, event); err != nil {
			panic(err)
		}
		var builds []*GitBuild
		for _, build := range event.Builds {
			builds = append(builds, &GitBuild{
				ID:     build.ID,
				Stage:	build.Stage,
				Name:	build.Name,
				Status:	build.Status,
			})
		}
		result := &GitPipelineEvent{
			ObjectKind:   event.ObjectKind,
			UserName:     event.UserName,
			UserUserName: event.UserUserName,
			ProjectID:    event.ProjectID,
			ProjectName:  event.ProjectName,
			Branch:       event.Branch,
			Commit:       GitCommit{
				ID:          event.Commit.ID,
				Message:     event.Commit.Message,
				Timestamp:   event.Commit.Timestamp,
				AuthorName:  event.Commit.AuthorName,
				AuthorEmail: event.Commit.AuthorEmail,
			},
			Builds:       builds,
		}
		return result, nil
	default:
		return nil, fmt.Errorf("can't parse event %v", r.Header.Get("Event-Type"))
	}
}
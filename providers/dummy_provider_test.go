package providers

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"
	"time"
)

func setupDummy() *FakeProvider {
	return NewFakeProvider("test_data/fake/project.json").(*FakeProvider)
}

func setupDummyWebhook(event string) (*http.Request, []byte) {
	request, _ := http.NewRequest("POST", "", nil)
	request.Header.Add("Event-Type", event)

	payload, _ := ioutil.ReadFile(fmt.Sprintf("test_data/fake/%v.json", event))

	return request, payload
}

func TestFakeProvider_GetIssue(t *testing.T) {
	prov := setupDummy()

	tests := []*GitIssue {
		{
			3,
			2,
			"Ut commodi ullam eos dolores perferendis nihil sunt.",
			"Omnis vero earum sunt corporis dolor et placeat.",
			"open",
			[]string{},
			"root",
		},
	}

	for _, test := range tests {
		issue, _ := prov.GetIssue(3, 2)

		if !reflect.DeepEqual(test, issue) {
			t.Errorf("Issues.GetIssue returned %+v, want %+v", issue, test)
		}
	}
}

func TestFakeProvider_GetLabels(t *testing.T) {
	prov := setupDummy()

	test := []*GitLabel {
		{
			3,
			"Todo",
			"#d9534f",
			"",
		},
		{
			3,
			"Stage",
			"#d9534f",
			"",
		},
		{
			3,
			"Doing",
			"#d9534f",
			"",
		},
	}

	labels, _ := prov.GetLabels(3)

	if !reflect.DeepEqual(test, labels) {
		t.Errorf("DummyProvider.GetLabels returned %+v, want %+v", labels, test)
	}
}

func TestFakeProvider_EditIssueLabel(t *testing.T) {
	prov := setupDummy()

	tests := []*GitIssue {
		{
			3,
			2,
			"Ut commodi ullam eos dolores perferendis nihil sunt.",
			"Omnis vero earum sunt corporis dolor et placeat.",
			"open",
			[]string{
				"Doing",
			},
			"root",
		},
	}

	label := []string{"Doing"}

	for _, test := range tests {
		issue, _ := prov.EditIssueLabel(3, 2, label)

		if !reflect.DeepEqual(test, issue) {
			t.Errorf("DummyProvider.GetIssue returned %+v, want %+v", issue, test)
		}
	}
}

func TestFakeProvider_ParseWebhook(t *testing.T) {
	prov := setupDummy()
	request, payload := setupDummyWebhook("push")
	timestamp1, _ := time.Parse(time.RFC3339, "2011-12-12T14:27:31+02:00")
	timestamp2, _ := time.Parse(time.RFC3339, "2012-01-03T23:36:29+02:00")


	testPush := &GitPushEvent{
		ObjectKind:  "push",
		Before: 	 "95790bf891e76fee5e1747ab589903a6a1f80f22",
		CheckoutSHA: "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
		UserID:      4,
		UserName:    "John Smith",
		ProjectID:   3,
		ProjectName: "Diaspora",
		Branch:      "refs/heads/develop",
		Commits:     []*GitCommit {
			{
				"b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327",
				"Update Catalan translation. Close #1, related to #2",
				&timestamp1,
				"Jordi Mallach",
				"jordi@softcatala.org",
			},
			{
				"da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
				"fixed readme, Close #2, #3",
				&timestamp2,
				"GitLab dev user",
				"gitlabdev@dv6700.(none)",
			},
		},
	}

	testPipeline := &GitPipelineEvent{
		ObjectKind:   "pipeline",
		UserName:     "Administrator",
		UserUserName: "root",
		ProjectID:    4,
		ProjectName:  "Diaspora",
		Branch:       "develop",
		Commit:       GitCommit{
			ID:          "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
			Message:     "Update Catalan translation. Close #1, related to #2",
			Timestamp:   &timestamp1,
			AuthorName:  "Jordi Mallach",
			AuthorEmail: "jordi@softcatala.org",
		},
		Builds:       []*GitBuild{
			{
				380,
				"deploy",
				"production",
				"success",
			},
			{
				377,
				"test",
				"test-image",
				"success",
			},
			{
				378,
				"test",
				"test-build",
				"success",
			},
			{
				376,
				"build",
				"build-image",
				"success",
			},
			{
				379,
				"deploy",
				"staging",
				"created",
			},
		},
	}

	event, _ := prov.ParseWebhook(request, payload)

	if !reflect.DeepEqual(testPush, event) {
		t.Errorf("DummyProvider.ParseWebhook returned %+v, want %+v", event, testPush)
	}

	request, payload = setupDummyWebhook("pipeline")

	event, _ = prov.ParseWebhook(request, payload)

	if !reflect.DeepEqual(testPipeline, event) {
		t.Errorf("DummyProvider.ParseWebhook returned %+v, want %+v", event, testPipeline)
	}

}
package providers

import "time"

type (

	GitIssue struct {
		PID       int
		Number    int
		Title     string
		Body      string
		State     string
		Labels    []string
		User      string
	}

	GitLabel struct {
		Repo        int
		Name        string
		Color       string
		Description string
	}

	GitCommit struct {
		ID			string
		Message		string
		Timestamp	*time.Time
		AuthorName	string
		AuthorEmail	string
	}

	GitPushEvent struct {
		ObjectKind	string
		Before		string
		CheckoutSHA	string
		UserID		int
		UserName	string
		ProjectID	int
		ProjectName	string
		Branch		string
		Commits 	[]*GitCommit
	}

	GitBuild struct {
		ID		int
		Stage	string
		Name	string
		Status	string
	}

	GitPipelineEvent struct {
		ObjectKind		string
		UserName		string
		UserUserName	string
		ProjectID		int
		ProjectName		string
		Branch			string
		Commit			GitCommit
		Builds			[]*GitBuild
	}
)

func StringInSlice(l string , labels []string) bool {
	for _, label := range labels {
		if l == label {
			return true
		}
	}
	return false
}

func PopStringFromSlice(l string, labels []string) []string {
	var result []string
	for _, label := range labels {
		if l != label {
			result = append(result, label)
		}
	}
	return result
}
